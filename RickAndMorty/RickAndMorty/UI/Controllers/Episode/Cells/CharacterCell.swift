//
//  CharacterCell.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 26.08.2023.
//

import UIKit

final class CharacterCell: UITableViewCell {
    private let mainView = UIView()
    
    private let iconImageView = CustomImageView()
    private let nameLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with: Result) {
        iconImageView.fetchImage(from: with.image ?? "")
        nameLabel.text = with.name ?? ""
    }
}

private extension CharacterCell {
    private func setupView() {
        mainView.backgroundColor = RMColor.darkGray
        mainView.layer.cornerRadius = Sizes.cornerRadius
        
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.layer.masksToBounds = true
        iconImageView.layer.cornerRadius = Sizes.cornerRadius
        
        nameLabel.moreSettings(textColor: RMColor.white, boldFont: true, fontSize: Sizes.normalFontSize, alignment: .center, numberOfLine: 0)
        
        contentView.addSubview(mainView)
        subviewOnView(iconImageView, nameLabel)
        constraints()
    }
    
    private func subviewOnView(_ subviews: UIView...) {
        subviews.forEach { subview in
            mainView.addSubview(subview)
        }
    }
}

private extension CharacterCell {
    private func constraints() {
        [mainView, iconImageView, nameLabel].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            mainView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            mainView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            mainView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Sizes.Spacing.S.top),
            mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            mainView.heightAnchor.constraint(equalToConstant: Sizes.Padding.L.height),
            
            iconImageView.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.S.top),
            iconImageView.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.S.left),
            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor, multiplier: Sizes.Padding.XL.multiplier),
            iconImageView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            
            nameLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.M.top),
            nameLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: Sizes.Spacing.M.left),
            nameLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            nameLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom)
        ])
    }
}
