//
//  EpisodeViewController.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 26.08.2023.
//

import UIKit

final class EpisodeViewController: UIViewController {
    private let progressIndicator = UIActivityIndicatorView(style: .medium)

    private let mainView = UIView()
    
    private let nameEpisodeLabel = UILabel()
    private let airDateLabel = UILabel()
    private let numberEpisodeLabel = UILabel()
    
    private var viewModel = EpisodeViewModel()
    private var episodeModel: RMEpisodeModel?
    private var characterModel: Result?
    
    private let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func inicializeVC(url: String) {
        progressIndicator.startAnimating()
        viewModel.fetchEpisodeData(url: url) { [weak self] result in
            guard let self = self else { return }
            self.episodeModel = result
            self.progressIndicator.stopAnimating()
            self.nameEpisodeLabel.text = result.name
            self.airDateLabel.text = result.airDate
            self.numberEpisodeLabel.text = result.episode.formatEpisodeString()
            self.tableView.reloadData()
        }
    }
    
    @objc private func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
}

extension EpisodeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerLabel = UILabel()
        headerLabel.moreSettings(text: "    Characters", textColor: RMColor.white, boldFont: true)
        return headerLabel
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let characterUrl = episodeModel?.characters[indexPath.row] else { return }
        fetchDataCharacter(url: characterUrl) { result in
            let vc = CharacterViewController()
//            vc.characterModel = result
            self.present(vc, animated: true)
        }
    }
}

extension EpisodeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return episodeModel?.characters.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CharacterCell.description(), for: indexPath) as! CharacterCell
        guard let characterUrl = episodeModel?.characters[indexPath.row] else { return cell }
        
        fetchDataCharacter(url: characterUrl) { result in
            cell.configure(with: result)
        }
        return configureCell(cell: cell)
    }
}

private extension EpisodeViewController {
    private func setupView() {
        view.backgroundColor = RMColor.darkBlue
        
        mainView.backgroundColor = RMColor.darkGray
        mainView.layer.cornerRadius = Sizes.cornerRadius
        setupElements()
        setupNavigationBar()
        setupTableView()
    }
    
    private func setupElements() {
        progressIndicator.color = RMColor.dirtGreen
        
        nameEpisodeLabel.moreSettings(boldFont: true, alignment: .center)
        numberEpisodeLabel.moreSettings(textColor: RMColor.dirtGreen, fontSize: Sizes.normalFontSize)
        airDateLabel.moreSettings(textColor: RMColor.gray, fontSize: Sizes.minFontSize, alignment: .right)
        
        subviewsOnView(mainView, progressIndicator, tableView)
        subviewsOnMainView(nameEpisodeLabel, airDateLabel, numberEpisodeLabel)
        constraints()
    }
    
    private func setupTableView() {
        tableView.backgroundColor = RMColor.AdditionalColor.clear
        tableView.separatorColor = RMColor.AdditionalColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        
        [CharacterCell.self].forEach { cell in
            tableView.register(cell, forCellReuseIdentifier: cell.description())
        }
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.tintColor = .white
        navigationItem.largeTitleDisplayMode = .never
        let backButtonImage = UIImage(systemName: "arrow.left")?.withRenderingMode(.alwaysTemplate)
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action: #selector(backButtonTapped))
        
        navigationItem.leftBarButtonItem = backButton
        navigationItem.backBarButtonItem = nil
    }
    
    private func configureCell<T: UITableViewCell>(cell: T) -> T {
        cell.backgroundColor = RMColor.AdditionalColor.clear
        cell.selectionStyle = .none
        return cell
    }
    
    private func fetchDataCharacter(url: String, completon: @escaping(Result) -> ()) {
        viewModel.fetchDataCharacter(url: url) { result in
            completon(result)
        }
    }
}

private extension EpisodeViewController {
    private func subviewsOnMainView(_ subviews: UIView...) {
        subviews.forEach { subview in
            mainView.addSubview(subview)
        }
    }
    
    private func subviewsOnView(_ subviews: UIView...) {
        subviews.forEach { subview in
            view.addSubview(subview)
        }
    }
    
    private func constraints() {
        [mainView, progressIndicator, nameEpisodeLabel, airDateLabel, numberEpisodeLabel, tableView].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: Sizes.Spacing.S.top),
            mainView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            mainView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            mainView.heightAnchor.constraint(equalToConstant: Sizes.Padding.L.height),
            
            progressIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            progressIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            progressIndicator.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
            progressIndicator.widthAnchor.constraint(equalToConstant: Sizes.Padding.M.width),
            
            nameEpisodeLabel.centerXAnchor.constraint(equalTo: mainView.centerXAnchor),
            nameEpisodeLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.S.top),
            nameEpisodeLabel.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            
            numberEpisodeLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            numberEpisodeLabel.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.M.left),
            numberEpisodeLabel.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: Sizes.Padding.S.multiplier),
            
            airDateLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            airDateLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            airDateLabel.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: Sizes.Padding.S.multiplier),
            
            tableView.topAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.S.top),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: Sizes.Padding.XL.multiplier),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
