//
//  EpisodeViewModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 26.08.2023.
//

import Foundation

class EpisodeViewModel {
    private var network = Network.shared
    
    private var episode: RMEpisodeModel!
    
    func fetchEpisodeData(url: String, completion: @escaping(RMEpisodeModel) -> ()) {
        network.fetchData(url: url) { (result: RMEpisodeModel) in
            completion(result)
        }
    }
    
    func fetchDataCharacter(url: String, completion: @escaping(Result) -> ()) {
        network.fetchData(url: url) { (result: Result) in
            completion(result)
        }
    }
}
