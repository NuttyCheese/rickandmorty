//
//  ListCharacterViewController.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 21.08.2023.
//

import UIKit

final class ListCharacterViewController: UIViewController {
    private let progressIndicator = UIActivityIndicatorView(style: .medium)
    private var collectionView: UICollectionView!
    
    private var viewModel = ListCharactersViewModel()
    
    private var charactersModel: RMCharactersModel?
    private var previousPageData: String = ""
    
    private enum LayoutConstant {
        static let spacing: CGFloat = Sizes.spacingItems
        static let itemHeight: CGFloat = Sizes.itemHeight
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @objc private func pressedRightItem() {
        guard let next = charactersModel?.info.next else { return }
        if !next.isEmpty {
            fetchData(url: next)
        }
    }
    
    @objc private func pressedLeftItem() {
        guard let prev = charactersModel?.info.prev else { return }
        if !prev.isEmpty {
            fetchData(url: prev)
        }
    }
}

extension ListCharacterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return charactersModel?.results?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CharacterItemCell.description(), for: indexPath) as! CharacterItemCell
        
        guard let character = charactersModel?.results?[indexPath.item] else { return cell }
        cell.backgroundColor = RMColor.darkGray
        cell.configure(with: character)
        cell.layer.cornerRadius = Sizes.cornerRadius
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let character = charactersModel?.results?[indexPath.item] else { return }
        let infoVC = CharacterViewController()
//        infoVC.characterModel = character
        navigationController?.pushViewController(infoVC, animated: true)
    }
}

extension ListCharacterViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = itemWidth(for: view.frame.width, spacing: LayoutConstant.spacing)
        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }
    
    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = 2
        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth = (width - totalSpacing) / itemsInRow
        return finalWidth
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: LayoutConstant.spacing, bottom: LayoutConstant.spacing, right: LayoutConstant.spacing)
    }
}

private extension ListCharacterViewController {
    private func setupView() {
        setupCollectionView()
        progressIndicator.color = RMColor.dirtGreen
        view.addSubview(progressIndicator)
        view.addSubview(collectionView)
        fetchData()
        registerCell()
        setupNavigationController()
        constraints()
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = RMColor.darkBlue
        collectionView.frame = view.bounds
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setupNavigationController() {
        title = "Characters"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: RMColor.white]
        

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(pressedRightItem))
        navigationItem.rightBarButtonItem?.tintColor = RMColor.white
    }
    
    private func isHiddenLeftBarButtonItem(_ url: String) {
        if !url.isEmpty {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Prev", style: .plain, target: self, action: #selector(pressedLeftItem))
            navigationItem.leftBarButtonItem?.tintColor = RMColor.white
        } else {
            navigationItem.leftBarButtonItem = nil
        }
    }
    
    
    private  func registerCell() {
        collectionView.register(CharacterItemCell.self, forCellWithReuseIdentifier: CharacterItemCell.description())
    }
    
    private func fetchData(url: String = "") {
        progressIndicator.startAnimating()
        collectionView.alpha = 0
        viewModel.fetchData (url: url) { [weak self] result in
            guard let self = self else { return }
            self.charactersModel = result
            self.isHiddenLeftBarButtonItem(result.info.prev ?? "")
            progressIndicator.stopAnimating()
            self.collectionView.alpha = 1
            self.collectionView.reloadData()
        }
    }
  
    private func constraints() {
        [progressIndicator, collectionView].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            progressIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            progressIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            progressIndicator.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
            progressIndicator.widthAnchor.constraint(equalToConstant: Sizes.Padding.M.width),
            
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

