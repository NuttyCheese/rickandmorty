//
//  CharacterItemCell.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 21.08.2023.
//

import UIKit

final class CharacterItemCell: UICollectionViewCell {
    private let characterImageView = CustomImageView()
    private let fullNameLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        constraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with: Result) {
        characterImageView.fetchImage(from: with.image ?? "")
        fullNameLabel.text = with.name
    }
}

private extension CharacterItemCell {
    private func setupView() {
        characterImageView.contentMode = .scaleAspectFit
        characterImageView.layer.masksToBounds = true
        characterImageView.layer.cornerRadius = Sizes.cornerRadius
        
        fullNameLabel.moreSettings(textColor: RMColor.white, boldFont: true, fontSize: Sizes.normalFontSize, alignment: .center)
        
        addSubview(characterImageView)
        addSubview(fullNameLabel)
    }
    
    private func constraints() {
        [characterImageView, fullNameLabel].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            characterImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Sizes.Spacing.S.top),
            characterImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Sizes.Spacing.S.left),
            characterImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: Sizes.Spacing.S.right),
            characterImageView.heightAnchor.constraint(equalToConstant: Sizes.Padding.XL.height),
            characterImageView.widthAnchor.constraint(equalToConstant: Sizes.Padding.XL.width),
            
            fullNameLabel.topAnchor.constraint(equalTo: characterImageView.bottomAnchor, constant: Sizes.Spacing.M.top),
            fullNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            fullNameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Sizes.Spacing.S.left),
            fullNameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: Sizes.Spacing.S.right)
        ])
    }
}
