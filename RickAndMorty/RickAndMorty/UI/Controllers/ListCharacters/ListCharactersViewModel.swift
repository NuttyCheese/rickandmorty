//
//  ListCharactersViewModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 24.08.2023.
//

import Foundation

class ListCharactersViewModel {
    private let network = Network.shared
    
    func fetchData(url: String, completion: @escaping(RMCharactersModel) -> ()) {
        network.fetchData(url: url.isEmpty ? Link.urlRMCharacter.rawValue : url) { (result: RMCharactersModel) in
            completion(result)
        }
    }
}
