//
//  InfoCell.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 22.08.2023.
//

import UIKit

final class InfoCell: UITableViewCell {
    private let mainView = UIView()
    
    private let speciesLabelOne = UILabel()
    private let typeLabelOne = UILabel()
    private let genderLabelOne = UILabel()
    private let speciesLabelTwo = UILabel()
    private let typeLabelTwo = UILabel()
    private let genderLabelTwo = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with result: Result) {
        if ((result.type?.isEmpty) != nil) {
            typeLabelTwo.text = "None"
        }else {
            typeLabelTwo.text = result.type
        }
        
        if let species = Species(rawValue: result.species?.rawValue ?? "") {
            speciesLabelTwo.text = species.rawValue
        } else {
            speciesLabelTwo.text = "Unknown"
        }
        
        if let gender = Gender(rawValue: result.gender?.rawValue ?? "") {
            genderLabelTwo.text = gender.rawValue
        } else {
            genderLabelTwo.text = "Unknown"
        }
    }
}

private extension InfoCell {
    private func setupView() {
        mainView.backgroundColor = RMColor.darkGray
        mainView.layer.cornerRadius = Sizes.cornerRadius
        
        speciesLabelOne.moreSettings(text: "Species:", textColor: RMColor.gray, fontSize: Sizes.minFontSize)
        speciesLabelTwo.moreSettings(boldFont: true, fontSize: Sizes.minFontSize, alignment: .right)
        
        typeLabelOne.moreSettings(text: "Type:", textColor: RMColor.gray, fontSize: Sizes.minFontSize)
        typeLabelTwo.moreSettings(boldFont: true, fontSize: Sizes.minFontSize, alignment: .right)
        
        genderLabelOne.moreSettings(text: "Gender:", textColor: RMColor.gray, fontSize: Sizes.minFontSize)
        genderLabelTwo.moreSettings(boldFont: true, fontSize: Sizes.minFontSize, alignment: .right)
        
        contentView.addSubview(mainView)
        subviewOnView(speciesLabelOne, speciesLabelTwo, typeLabelOne, typeLabelTwo, genderLabelOne, genderLabelTwo)
        constraints()
    }
}

private extension InfoCell {
    private func subviewOnView(_ subviews: UIView...) {
        subviews.forEach { subview in
            mainView.addSubview(subview)
        }
    }
    private func constraints() {
        [mainView, speciesLabelOne, speciesLabelTwo, typeLabelOne, typeLabelTwo, genderLabelOne, genderLabelTwo].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            mainView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            mainView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            mainView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Sizes.Spacing.S.top),
            mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            
            speciesLabelOne.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.M.top),
            speciesLabelOne.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.M.left),
            speciesLabelOne.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            
            typeLabelOne.topAnchor.constraint(equalTo: speciesLabelOne.bottomAnchor, constant: Sizes.Spacing.M.top),
            typeLabelOne.leftAnchor.constraint(equalTo: speciesLabelOne.leftAnchor),
            typeLabelOne.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            
            genderLabelOne.topAnchor.constraint(equalTo: typeLabelOne.bottomAnchor, constant: Sizes.Spacing.M.top),
            genderLabelOne.leftAnchor.constraint(equalTo: speciesLabelOne.leftAnchor),
            genderLabelOne.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            genderLabelOne.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            
            speciesLabelTwo.topAnchor.constraint(equalTo: speciesLabelOne.topAnchor),
            speciesLabelTwo.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            speciesLabelTwo.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            speciesLabelTwo.leftAnchor.constraint(equalTo: speciesLabelOne.rightAnchor, constant: Sizes.Spacing.L.left),
            
            typeLabelTwo.topAnchor.constraint(equalTo: typeLabelOne.topAnchor),
            typeLabelTwo.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            typeLabelTwo.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            typeLabelTwo.leftAnchor.constraint(equalTo: typeLabelOne.rightAnchor, constant: Sizes.Spacing.L.left),
            
            genderLabelTwo.topAnchor.constraint(equalTo: genderLabelOne.topAnchor),
            genderLabelTwo.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            genderLabelTwo.heightAnchor.constraint(equalToConstant: Sizes.Padding.S.height),
            genderLabelTwo.leftAnchor.constraint(equalTo: genderLabelOne.rightAnchor, constant: Sizes.Spacing.L.left)
        ])
    }
}
