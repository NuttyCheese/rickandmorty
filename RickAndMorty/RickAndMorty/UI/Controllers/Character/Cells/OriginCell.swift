//
//  OriginCell.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 22.08.2023.
//

import UIKit

final class OriginCell: UITableViewCell {
    private let mainView = UIView()
    
    private let squareView = UIView()
    private let planetImageView = UIImageView()
    private let namePlanetLabel = UILabel()
    private let typeSkyBodyLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with: RMLocationModel) {
        planetImageView.image = UIImage(named: "Planet")
        namePlanetLabel.text = with.name
        typeSkyBodyLabel.text = with.type
    }
    
}

private extension OriginCell {
    private func setupView() {
        mainView.backgroundColor = RMColor.darkGray
        mainView.layer.cornerRadius = Sizes.cornerRadius
        
        squareView.backgroundColor = RMColor.datkSky
        squareView.layer.cornerRadius = Sizes.cornerRadius
        
        planetImageView.contentMode = .scaleAspectFit
        planetImageView.tintColor = RMColor.white
        
        namePlanetLabel.moreSettings(boldFont: true, fontSize: Sizes.normalFontSize)
        typeSkyBodyLabel.moreSettings(textColor: RMColor.dirtGreen, fontSize: Sizes.normalFontSize)
        
        contentView.addSubview(mainView)
        subviewOnView(squareView, namePlanetLabel, typeSkyBodyLabel)
        squareView.addSubview(planetImageView)
        constraints()
    }
    
    private func subviewOnView(_ subviews: UIView...) {
        subviews.forEach { subview in
            mainView.addSubview(subview)
        }
    }
}

private extension OriginCell {
    private func constraints() {
        [mainView, squareView, planetImageView, namePlanetLabel, typeSkyBodyLabel].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            mainView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            mainView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            mainView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Sizes.Spacing.S.top),
            mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            mainView.heightAnchor.constraint(equalToConstant: Sizes.Padding.L.height),
            
            squareView.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.S.top),
            squareView.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.S.left),
            squareView.widthAnchor.constraint(equalTo: squareView.heightAnchor, multiplier: Sizes.Padding.XL.multiplier),
            squareView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            
            planetImageView.centerXAnchor.constraint(equalTo: squareView.centerXAnchor),
            planetImageView.centerYAnchor.constraint(equalTo: squareView.centerYAnchor),
            planetImageView.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
            planetImageView.widthAnchor.constraint(equalToConstant: Sizes.Padding.XL.width),
            
            namePlanetLabel.topAnchor.constraint(equalTo: squareView.topAnchor, constant: Sizes.Spacing.S.top),
            namePlanetLabel.leftAnchor.constraint(equalTo: squareView.rightAnchor, constant: Sizes.Spacing.M.left),
            namePlanetLabel.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
            namePlanetLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            
            typeSkyBodyLabel.bottomAnchor.constraint(equalTo: squareView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            typeSkyBodyLabel.leftAnchor.constraint(equalTo: squareView.rightAnchor, constant: Sizes.Spacing.M.left),
            typeSkyBodyLabel.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
            typeSkyBodyLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
        ])
    }
}
