//
//  EpisodeCell.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 22.08.2023.
//

import UIKit

final class EpisodeCell: UITableViewCell {
    private let mainView = UIView()
    
    private let nameEpisodeLabel = UILabel()
    private let episodeSeasonLabel = UILabel()
    private let airDateLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with: RMEpisodeModel) {
        nameEpisodeLabel.text = with.name
        episodeSeasonLabel.text = with.episode.formatEpisodeString()
        airDateLabel.text = with.airDate
    }
}

private extension EpisodeCell {
    private func setupView() {
        mainView.backgroundColor = RMColor.darkGray
        mainView.layer.cornerRadius = Sizes.cornerRadius
        
        nameEpisodeLabel.moreSettings()
        episodeSeasonLabel.moreSettings(textColor: RMColor.dirtGreen, fontSize: Sizes.normalFontSize)
        airDateLabel.moreSettings(textColor: RMColor.gray, fontSize: Sizes.minFontSize, alignment: .right)
        
        contentView.addSubview(mainView)
        subviewOnView(nameEpisodeLabel, episodeSeasonLabel, airDateLabel)
        constraints()
    }
    
    private func subviewOnView(_ subviews: UIView...) {
        subviews.forEach { subview in
            mainView.addSubview(subview)
        }
    }
}

private extension EpisodeCell {
    private func constraints() {
        [mainView, nameEpisodeLabel, episodeSeasonLabel, airDateLabel].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            mainView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            mainView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: Sizes.Padding.L.multiplier),
            mainView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Sizes.Spacing.S.top),
            mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Sizes.Spacing.S.bottom),
            mainView.heightAnchor.constraint(equalToConstant: Sizes.Padding.L.height),
            
            nameEpisodeLabel.topAnchor.constraint(equalTo: mainView.topAnchor, constant: Sizes.Spacing.M.top),
            nameEpisodeLabel.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.M.left),
            nameEpisodeLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            
            episodeSeasonLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            episodeSeasonLabel.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: Sizes.Spacing.M.left),
            episodeSeasonLabel.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: Sizes.Padding.S.multiplier),
            
            airDateLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: Sizes.Spacing.M.bottom),
            airDateLabel.rightAnchor.constraint(equalTo: mainView.rightAnchor, constant: Sizes.Spacing.M.right),
            airDateLabel.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: Sizes.Padding.S.multiplier)
        ])
    }
}
