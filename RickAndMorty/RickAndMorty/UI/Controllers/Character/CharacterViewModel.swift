//
//  CharacterViewModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 24.08.2023.
//

import Foundation

class CharacterViewModel {
    private let network = Network.shared
    
    func fetchDataEpisode(url: String, completion: @escaping(RMEpisodeModel) -> ()) {
        network.fetchData(url: url) { (result: RMEpisodeModel) in
            completion(result)
        }
    }
    
    func fetchDataLocation(url: String, completion: @escaping(RMLocationModel) -> ()) {
        network.fetchData(url: url) { (result: RMLocationModel) in
            completion(result)
        }
    }
}
