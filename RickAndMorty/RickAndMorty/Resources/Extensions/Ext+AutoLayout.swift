//
//  Ext+AutoLayout.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

public enum Position {
    case top
    case bottom
    case center
}

public enum Position1 {
    case topLeft(_ shft: CGFloat = 10)
    case topRight(_ shft: CGFloat = -10)
    case topCenter(_ shft: CGFloat = 10)
    case bottomLeft(_ shft: CGFloat = 10)
    case bottomRight(_ shft: CGFloat = -10)
    case bottomCenter(_ shft: CGFloat = -10)
    case left(_ shft: CGFloat = 10)
    case right(_ shft: CGFloat = -10)
    case center
    case full(top: CGFloat = 10, left: CGFloat = 10, right: CGFloat = -10, bottom: CGFloat = -10)
}

extension UIView {
    private var superView: UIView { superview! }
    
    public func cell(_ position: Position) {
        tAMIC()
        
        var positions: [NSLayoutConstraint] = [
            leftAnchor.constraint(equalTo: superView.leftAnchor, constant: Sizes.Spacing.S.left),
            rightAnchor.constraint(equalTo: superView.rightAnchor, constant: Sizes.Spacing.S.right)
        ]
        switch position {
        case .top:
            positions.insert(topAnchor.constraint(equalTo: superView.topAnchor, constant: Sizes.Spacing.S.top), at: 0)
        case .bottom:
            positions.insert(bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: Sizes.Spacing.S.bottom), at: 0)
        case .center:
            positions.insert(topAnchor.constraint(equalTo: superView.topAnchor, constant: Sizes.Spacing.S.top), at: 0)
            positions.insert(bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: Sizes.Spacing.S.bottom), at: 1)
        }
        
        NSLayoutConstraint.activate(positions)
    }
    
    public func fullScreen(_ view: UIView? = nil) {
        tAMIC()
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: view?.topAnchor ?? superView.topAnchor),
            leftAnchor.constraint(equalTo: view?.leftAnchor ?? superView.leftAnchor),
            rightAnchor.constraint(equalTo: view?.rightAnchor ??  superView.rightAnchor),
            bottomAnchor.constraint(equalTo: view?.bottomAnchor ?? superView.bottomAnchor)
        ])
    }
    
    public func fullScreenMarginsGuide() {
        tAMIC()
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superView.layoutMarginsGuide.topAnchor),
            leftAnchor.constraint(equalTo: superView.leftAnchor),
            rightAnchor.constraint(equalTo: superView.rightAnchor),
            bottomAnchor.constraint(equalTo: superView.layoutMarginsGuide.bottomAnchor)
        ])
    }
    
    public func positionCell(_ position: Position1) {
        
    }
    
    public func positionView(_ position: Position1, view: UIView? = nil, height: CGFloat? = nil, width: CGFloat? = nil) {
        tAMIC()
        var constraints = [NSLayoutConstraint]()
        
        if let height {
            constraints.insert(heightAnchor.constraint(equalToConstant: height), at: 0)
        }
        
        if let width {
            constraints.insert(widthAnchor.constraint(equalToConstant: width), at: 1)
        }
        
//        switch position {
//            case .topLeft(let shift):
//                <#code#>
//            case .topRight(let shift):
//                <#code#>
//            case .topCenter(let shift):
//                <#code#>
//            case .bottomLeft(let shift):
//                <#code#>
//            case .bottomRight(let shift):
//                <#code#>
//            case .bottomCenter(let shift):
//                <#code#>
//            case .left(let shift):
//                <#code#>
//            case .right(let shift):
//                <#code#>
//            case .center:
//                <#code#>
//            case .full(let top, let left, let right, let bottom):
//                <#code#>
//        }
        
        NSLayoutConstraint.activate(constraints)
    }
}
