//
//  Ext+Collection.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

extension Collection {
    /// Проверяет, является ли данная коллекция пустой.
    var isEmpty: Bool {
        return self.isEmpty
    }
    
    /// Проверяет, является ли данная коллекция не пустой.
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
}
