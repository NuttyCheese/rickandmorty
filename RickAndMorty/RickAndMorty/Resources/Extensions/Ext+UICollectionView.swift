//
//  Ext+UICollectionView.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

extension UICollectionView {
    /// Регистрирует множество различных ячеек в коллекции.
    ///
    /// Каждая переданная ячейка регистрируется с использованием ее типа в качестве идентификатора.
    ///
    /// - Parameter cells: Типы ячеек для регистрации.
    public func registeringCellsInCollection(_ cells: UICollectionViewCell.Type...) {
        cells.forEach { cell in
            self.register(cell, forCellWithReuseIdentifier: cell.description())
        }
    }
}

