//
//  Ext+UIImage.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

enum ImageFilterStorage {
    ///150w
    case superLow
    ///250w
    case low
    ///600w
    case medium
    ///1200w
    case high
    ///1600w
    case superHigh
    
    func getDescription() -> String {
        switch self {
            case .superLow: "150w"
            case .low: "250w"
            case .medium: "600w"
            case .high: "1200w"
            case .superHigh: "1600w"
        }
    }
}

extension UIImage {
    /**
        Удобный инициализатор для создания UIImage с однородным цветом.

        - Parameter color: Цвет, которым будет заполнено изображение.

        - Returns: UIImage, заполненное указанным цветом.
    */
    convenience init(color: UIColor) {
        let size = CGSize(width: 1, height: 1)
        let renderer = UIGraphicsImageRenderer(size: size)
        let img = renderer.image { context in
            color.setFill()
            context.fill(CGRect(origin: .zero, size: size))
        }
        self.init(cgImage: img.cgImage!)
    }
    /**
        Асинхронно загружает изображение по указанному URL-адресу.

        - Parameters:
            - urlString: Строка URL-адреса, по которой будет загружено изображение.
            - completion: Замыкание, которое будет вызвано после завершения операции загрузки изображения. Замыкание принимает единственный параметр типа UIImage?, который представляет загруженное изображение. Если изображение не удалось загрузить, в замыкание передается nil.

        - Note: Если изображение не удалось загрузить, в качестве плейсхолдера будет передано изображение с именем "not_photo".
    */
//    static func loadImage(from urlString: String, quality: ImageFilterStorage = .medium, completion: @escaping (UIImage?) -> Void) {
//        guard let url = URL(string: Settings.vimosStorage + "/filter/\(quality.getDescription())" + urlString) else {
//            completion(nil)
//            return
//        }
//        
//        if let cachedImage = ImageCache.shared.image(forKey: urlString) {
//            DispatchQueue.main.async {
//                completion(cachedImage)
//            }
//        } else {
//            URLSession.shared.dataTask(with: url) { data, response, error in
//                if let error = error {
//                    print("Ошибка загрузки изображения: \(error)")
//                    DispatchQueue.main.async {
//                        completion(UIImage(named: "not_photo"))
//                    }
//                    return
//                }
//                
//                if let data = data, let image = UIImage(data: data) {
//                    DispatchQueue.main.async {
//                        ImageCache.shared.save(image, forKey: urlString)
//                        completion(image)
//                    }
//                } else {
//                    DispatchQueue.main.async {
//                        completion(UIImage(named: "not_photo"))
//                    }
//                }
//            }.resume()
//        }
//    }
    
    func resize(to size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        defer { UIGraphicsEndImageContext() }
        
        draw(in: CGRect(origin: .zero, size: size))
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
