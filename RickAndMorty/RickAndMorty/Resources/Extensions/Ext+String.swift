//
//  Ext+String.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

extension String {
    ///Конвертирует полученую строку из **S01E01** в удобочитаемую **Episode: 01, Season: 01**
    func formatEpisodeString() -> String? {
        let pattern = #"S(\d+)E(\d+)"#
        let regex = try! NSRegularExpression(pattern: pattern)
        
        if let match = regex.firstMatch(in: self, range: NSRange(self.startIndex..., in: self)) {
            let season = (self as NSString).substring(with: match.range(at: 1))
            let episode = (self as NSString).substring(with: match.range(at: 2))
            return "Episode: \(episode), Season \(season)"
        }
        return nil
    }
}
