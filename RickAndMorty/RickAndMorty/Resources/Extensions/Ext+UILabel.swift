//
//  Ext+UILabel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

extension UILabel {
    ///Кастомное расширение для UILabel
    ///   - Parameters:
    ///      - text: Передает название текста, значение по умолчанию задано **пустым**
    ///      - textColor: Передает цвет текста, значение по умолчанию задано **(Белый цвет)**
    ///      - boldFont: Передает булевое условие для значения **fontSize**, по умолчание **Ложно**, если **Истино**, то текст будет "жирным"
    ///      - fontSize: Передает размер текста, значение по умолчанию задано **(22)**
    ///      - adjustFont:Передает метод **adjustsFontSizeToFitWidth** с значением по умолачние **Истино**
    ///      - alignment: Передает метод **textAlignment** с значением по умолчанию **.left**
    ///      - numberOfLine: Передает метод **numberOfLines** с значением по умолчанию **Единица**
    func moreSettings(text: String = "", textColor: UIColor = RMColor.white, boldFont: Bool = false, fontSize: CGFloat = Sizes.maxFontSize, adjustFont: Bool = true, alignment: NSTextAlignment = .left, numberOfLine: Int = 1) {
        self.text = text
        self.textColor = textColor
        if boldFont {
            self.font = UIFont.boldSystemFont(ofSize: fontSize)
        }else {
            self.font = UIFont.systemFont(ofSize: fontSize)
        }
        self.adjustsFontSizeToFitWidth = adjustFont
        self.textAlignment = alignment
        self.numberOfLines = numberOfLine
        self.sizeToFit()
    }
}
