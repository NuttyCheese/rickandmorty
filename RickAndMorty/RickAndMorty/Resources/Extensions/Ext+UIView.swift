//
//  Ext+UIView.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

extension UIView {
    /// Устанавливает свойство `isUserInteractionEnabled` в `true`.
    public func enable() {
        self.isUserInteractionEnabled = true
    }
    /// Устанавливает свойство `isUserInteractionEnabled` в `false`.
    public func disable() {
        self.isUserInteractionEnabled = false
    }
    /// Устанавливает свойство `translatesAutoresizingMaskIntoConstraints` в `false`.
    public func tAMIC() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    /// Добавляет несколько подвью на текущее представление.
    ///
    /// - Parameter subviews: Массив подвью для добавления.
    public func subviewsOnView(_ subviews: UIView...) {
        subviews.forEach { addSubview($0) }
    }
    /// Вращает текущее представление.
    ///
    /// Представление вращается на 360 градусов в течение 3 секунд, повторяя анимацию бесконечно.
    public func rotate() {
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 3.0
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}

