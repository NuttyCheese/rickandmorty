//
//  SectionViewModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

struct SectionViewModel {
    let title: String
    let viewModels: [ICellViewModel]
}
