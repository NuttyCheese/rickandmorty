//
//  ModuleCollectionViewController.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

protocol IModuleCollectionView: AnyObject {
    func update(sections: [SectionViewModel])
}

class ModuleCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    public var sections = [SectionViewModel]()
    public var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: Sizes.Spacing.S.top,
                     left: Sizes.Spacing.S.left,
                     bottom: 1,
                     right: Sizes.Spacing.S.left)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        Sizes.Spacing.S.top
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        Sizes.Spacing.S.top
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
            collectionView.scrollIndicatorInsets = collectionView.contentInset
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        collectionView.contentInset = .zero
        collectionView.scrollIndicatorInsets = .zero
    }
}
extension ModuleCollectionViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard section < sections.count else { return 0 }
        return self.sections[section].viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard indexPath.section < sections.count,
              indexPath.row < sections[indexPath.section].viewModels.count,
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sections[indexPath.section].viewModels[indexPath.row].cellReuseID(),
                                                       for: indexPath) as? ICollectionViewCell else { return UICollectionViewCell() }
        cell.backgroundColor = .clear
        cell.configure(with: sections[indexPath.section].viewModels[indexPath.row])
        
        return cell
    }
}

private extension ModuleCollectionViewController {
    func setupView() {
        setupCollectionView()
        setupNotificationServices()
    }
    
    func setupNotificationServices() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .onDrag
        
//        collectionView.registeringCellsInCollection()
        view.subviewsOnView(collectionView)
        collectionView.fullScreen()
    }
}
