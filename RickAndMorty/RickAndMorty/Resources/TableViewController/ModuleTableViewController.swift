//
//  ModuleTableViewController.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

protocol IModuleTableView: AnyObject {
    func update(sections: [SectionViewModel])
}

class ModuleTableViewController: UIViewController, UITableViewDelegate {
    public var sections = [SectionViewModel]()
    public var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNotificationServices()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { }
    func scrollViewDidScroll(_ scrollView: UIScrollView) { }
    
    @objc private func keyboardWillShow(notification: Notification) {
        notification.userInfo
            .flatMap { $0[UIResponder.keyboardFrameEndUserInfoKey] }
            .flatMap { $0 as? NSValue }
            .map(\.cgRectValue.height)
            .map { UIEdgeInsets(top: 0, left: 0, bottom: $0, right: 0) }
            .map { insets in
                tableView.contentInset = insets
                tableView.scrollIndicatorInsets = insets
            }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        tableView.contentInset = .zero
        tableView.scrollIndicatorInsets = .zero
    }
}

private extension ModuleTableViewController {
    func setupNotificationServices() {
        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillShowNotification,
            object: nil,
            queue: .main,
            using: keyboardWillShow)
        
        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillHideNotification,
            object: nil,
            queue: .main,
            using: keyboardWillHide)
    }
    
    func setupTableView() {
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        
//        tableView.registeringCellsInTable()
        
        view.subviewsOnView(tableView)
        tableView.fullScreen()
    }
}
