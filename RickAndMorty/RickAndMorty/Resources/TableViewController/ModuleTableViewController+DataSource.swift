//
//  ModuleTableViewController+DataSource.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

extension ModuleTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < sections.count else { return 0 }
        return self.sections[section].viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section < sections.count,
              indexPath.row < sections[indexPath.section].viewModels.count,
              let cell = tableView.dequeueReusableCell(withIdentifier: sections[indexPath.section].viewModels[indexPath.row].cellReuseID(),
                                                       for: indexPath) as? ITableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.configure(with: sections[indexPath.section].viewModels[indexPath.row])
        
        return cell
    }
}
