//
//  Sections.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

enum Sections {
    case title(title: String)
    
    var text: String {
        switch self {
        case .title(let title):
            return title
        }
    }
}
