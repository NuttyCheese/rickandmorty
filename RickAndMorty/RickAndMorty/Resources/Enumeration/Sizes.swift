//
//  Sizes.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 24.08.2023.
//

import Foundation

enum Sizes {
    ///cornerRadius = 10
    static let cornerRadius: CGFloat = 10
    
    ///maxFontSize = 22
    static let maxFontSize: CGFloat = 22
    ///normalFontSize = 18
    static let normalFontSize: CGFloat = 18
    ///minFontSize = 16
    static let minFontSize: CGFloat = 16
    
    ///spacingItems = 16
    static let spacingItems: CGFloat = 16
    ///itemHeight = 210
    static let itemHeight: CGFloat = 210
    
    enum Spacing {
        ///S - 8
        enum S {
            static let top: CGFloat = 8
            static let left: CGFloat = 8
            static let right: CGFloat = -8
            static let bottom: CGFloat = -8
        }
        ///M - 16
        enum M {
            static let top: CGFloat = 16
            static let left: CGFloat = 16
            static let right: CGFloat = -16
            static let bottom: CGFloat = -16
        }
        ///L - 32
        enum L {
            static let top: CGFloat = 32
            static let left: CGFloat = 32
            static let right: CGFloat = -32
            static let bottom: CGFloat = -32
        }
    }
    
    enum Padding {
        ///height = 20
        ///multiplier = 0.4
        enum S {
            static let height: CGFloat = 20
            static let multiplier: CGFloat = 0.4
        }
        ///height = 30
        ///width = 30
        ///multiplier = 0.8
        enum M {
            static let height: CGFloat = 30
            static let width: CGFloat = 30
            static let multiplier: CGFloat = 0.8
        }
        ///height = 100
        ///multiplier = 0.9
        enum L {
            static let height: CGFloat = 100
            static let multiplier: CGFloat = 0.9
        }
        ///height = 150
        ///width  = 155
        ///multiplier = 1.0
        enum XL {
            static let height: CGFloat = 150
            static let width: CGFloat = 155
            static let multiplier: CGFloat = 1.0
        }
    }
}
