//
//  CellToolProtocol.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

protocol ITableViewCell where Self: UITableViewCell {
    func configure(with viewModel: ICellViewModel)
}

protocol ICollectionViewCell where Self: UICollectionViewCell {
    func configure(with viewModel: ICellViewModel)
}
