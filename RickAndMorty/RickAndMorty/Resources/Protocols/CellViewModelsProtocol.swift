//
//  CellViewModelsProtocol.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

protocol ICellViewModel {
    func cellReuseID() -> String
}
