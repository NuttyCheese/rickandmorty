//
//  Color.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 21.08.2023.
//

import UIKit

enum RMColor {
    static let darkBlue = UIColor(hex: "030c1e")
    static let darkGray = UIColor(hex: "262a38")
    static let white = UIColor(hex: "ffffff")
    static let dirtGreen = UIColor(hex: "46be0e")
    static let gray = UIColor(hex: "7a7f85")
    static let datkSky = UIColor(hex: "191C2A")
    
    enum AdditionalColor {
        static let clear = UIColor.clear
    }
}
