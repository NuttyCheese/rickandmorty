//
//  Network.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 21.08.2023.
//

import Foundation

enum Link: String {
    case urlRMCharacter = "https://rickandmortyapi.com/api/character"
}

class Network {
    static let shared = Network()
    ///Универсальный метод для выполнения сететвых запросов и обработки разных моделей
    /// - Parameters:
    ///     - T(Codable): Тип модели подписанный на Сodable
    ///     - url: Ссылка в виде строки
    ///     - completion: Передает данные по модели
    func fetchData<T: Codable>(url: String, completion: @escaping (T) -> ()) {
        guard let url = URL(string: url) else { return }
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data else {
                print(error ?? "error")
                return
            }
            let decoder = JSONDecoder()
            do {
                let result = try decoder.decode(T.self, from: data)
                DispatchQueue.main.async {
                    completion(result)
                }
            }catch {
                print(error)
            }
        }.resume()
    }
    
    private init() {}
}

class ImageManager {
    static var shared = ImageManager()
    ///Универсальный метод для выполнения сететвых запросов и обработки изображения
    /// - Parameters:
    ///     - url: Ссылка в виде строки
    ///     - completion: Передает данные
    func fetchImage(from url: URL, completion: @escaping(Data, URLResponse) ->Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let response = response else {
                print(error ?? "error")
                return
            }
            guard url == response.url else { return }
            
            DispatchQueue.main.async {
                completion(data, response)
            }
        }.resume()
    }
    
    private init() {}
}
