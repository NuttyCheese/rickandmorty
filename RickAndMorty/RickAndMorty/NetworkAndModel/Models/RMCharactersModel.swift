//
//  RMCharactersModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 21.08.2023.
//

import Foundation

struct RMCharactersModel: Codable {
    let info: Info
    let results: [Result]?
}
// MARK: - Info
struct Info: Codable {
    let count: Int
    let pages: Int
    let next: String
    let prev: String?
}

// MARK: - Result
struct Result: Codable {
    let id: Int?
    let name: String?
    let status: Status?
    let species: Species?
    let type: String?
    let gender: Gender?
    let origin: Location?
    let location: Location?
    let image: String?
    let episode: [String]?
    let url: String?
    let created: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        status = try container.decodeIfPresent(Status.self, forKey: .status)
        type = try container.decodeIfPresent(String.self, forKey: .type)
        image = try container.decodeIfPresent(String.self, forKey: .image)
        episode = try container.decodeIfPresent([String].self, forKey: .episode)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        created = try container.decodeIfPresent(String.self, forKey: .created)
        
        do {
            species = try container.decodeIfPresent(Species.self, forKey: .species)
        } catch {
            species = nil
        }
        
        do {
            origin = try container.decodeIfPresent(Location.self, forKey: .origin)
        } catch {
            origin = nil
        }
        
        do {
            location = try container.decodeIfPresent(Location.self, forKey: .location)
        } catch {
            location = nil
        }
        
        do {
            gender = try container.decodeIfPresent(Gender.self, forKey: .gender)
        } catch {
            gender = nil
        }
    }
}


enum Gender: String, Codable {
    case female = "Female"
    case male = "Male"
    case unknown = "unknown"
}

// MARK: - Location
struct Location: Codable {
    let name: String?
    let url: String?
}

enum Species: String, Codable {
    case alien = "Alien"
    case human = "Human"
}

enum Status: String, Codable {
    case alive = "Alive"
    case dead = "Dead"
    case unknown = "unknown"
}
