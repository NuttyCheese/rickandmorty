//
//  RMLocationModel.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 23.08.2023.
//

import Foundation

struct RMLocationModel: Codable {
    let id: Int
    let name, type, dimension: String
    let residents: [String]
    let url: String
    let created: String
}



