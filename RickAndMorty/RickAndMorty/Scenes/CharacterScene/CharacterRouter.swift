//
//  CharacterRouter.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

protocol ICharacterRouter {
    
}

final class CharacterRouter {
    weak var transitionHandler: UIViewController?
}

extension CharacterRouter: ICharacterRouter {
    
}
