//
//  CharacterPresenter.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

protocol ICharacterPresenter {
    func publish(_ data: CharacterModel.Response)
}

final class CharacterPresenter {
    weak var viewController: ICharacterView?
}

extension CharacterPresenter: ICharacterPresenter {
    func publish(_ data: CharacterModel.Response) {
        let itemViewModels = data.items.map(mapData)
        viewController?.update(sections: [SectionViewModel(title: "", viewModels: itemViewModels)])
    }
}

private extension CharacterPresenter {
    func mapData(_ item: CharacterModel.Response.Item) -> ICellViewModel {
        
    }
}
