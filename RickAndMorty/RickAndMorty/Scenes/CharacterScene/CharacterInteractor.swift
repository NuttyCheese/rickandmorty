//
//  CharacterInteractor.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import Foundation

enum SectionTitle: String, CaseIterable {
    case one = "Info"
    case two = "Origin"
    case three = "Episodes"
    static var allTitles = [one, two, three]
}

protocol ICharacterInteractor {
    func loadData()
}

final class CharacterInteractor {
    var presenter: ICharacterPresenter?
    var router: ICharacterRouter?
    
    private var items = [CharacterModel.Response.Item]()
}

extension CharacterInteractor: ICharacterInteractor {
    func loadData() {
        items = [
        
        ]
        
        presenter?.publish(CharacterModel.Response(items: items))
    }
}
