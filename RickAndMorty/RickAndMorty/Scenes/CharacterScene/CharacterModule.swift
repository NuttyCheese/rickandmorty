//
//  CharacterModule.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

enum CharacterModule {
    static func build() -> UIViewController {
        let view = CharacterViewController()
        let interactor = CharacterInteractor()
        let presenter = CharacterPresenter()
        let router = CharacterRouter()
        
        view.interactor = interactor
        
        interactor.presenter = presenter
        interactor.router = router
        
        presenter.viewController = view
        
        router.transitionHandler = view
        
        return view
    }
}
