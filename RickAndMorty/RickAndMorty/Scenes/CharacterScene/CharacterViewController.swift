//
//  CharacterViewController.swift
//  RickAndMorty
//
//  Created by Борис Павлов on 16.06.2024.
//

import UIKit

protocol ICharacterView: IModuleTableView {
    
}

final class CharacterViewController: ModuleTableViewController {
    var interactor: ICharacterInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interactor?.loadData()
    }
}

extension CharacterViewController: ICharacterView {
    func update(sections: [SectionViewModel]) {
        self.sections = sections
        tableView.reloadData()
    }
}

//final class CharacterViewController: UIViewController {
//    private let progressIndicator = UIActivityIndicatorView(style: .medium)
//
//    private let iconImageView = CustomImageView()
//    private let fullNameLabel = UILabel()
//    private let statusLabel = UILabel()
//
//    private let tableView = UITableView()
//
//    private let viewModel = CharacterViewModel()
//    private let modelEpisode = EpisodeViewModel()
//
//    var characterModel: Result!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setupView()
//    }
//
//    @objc private func backButtonTapped() {
//        navigationController?.popViewController(animated: true)
//    }
//}
//
//extension CharacterViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerLabel = UILabel()
//        headerLabel.moreSettings(text: "     \(SectionTitle.allTitles[section].rawValue)", textColor: RMColor.white, boldFont: true)
//        return headerLabel
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        guard let episode = characterModel.episode?[indexPath.row] else { return }
//        if indexPath.section == 2 {
//            let vc = EpisodeViewController()
//            vc.inicializeVC(url: episode)
//            navigationController?.pushViewController(vc, animated: true)
//        }
//    }
//}
//
//extension CharacterViewController: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return SectionTitle.allTitles.count
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0: return 1
//        case 1: return 1
//        case 2: return characterModel.episode?.count ?? 0
//        default: return 0
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        progressIndicator.startAnimating()
//        switch indexPath.section {
//        case 0:
//            let cell = tableView.dequeueReusableCell(withIdentifier: InfoCell.description(), for: indexPath) as! InfoCell
//            cell.configure(with: characterModel)
//            return configureCell(cell: cell)
//        case 1:
//            let cell = tableView.dequeueReusableCell(withIdentifier: OriginCell.description(), for: indexPath) as! OriginCell
//            fetchDataLocation(url: (characterModel.origin?.url?.isEmpty ?? true ? characterModel.location?.url : characterModel.origin?.url) ?? "") { result in
//                cell.configure(with: result)
//            }
//            return configureCell(cell: cell)
//        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: EpisodeCell.description(), for: indexPath) as! EpisodeCell
//            guard let episodeUrl = characterModel.episode?[indexPath.row] else { return cell }
//
//            fetchDataEpisode(url: episodeUrl) { episode in
//                cell.configure(with: episode)
//            }
//            progressIndicator.stopAnimating()
//            return configureCell(cell: cell)
//        default:
//            return UITableViewCell()
//        }
//    }
//}
//
//private extension CharacterViewController {
//    private func setupView() {
//        view.backgroundColor = RMColor.darkBlue
//        setupElement()
//        setupTableView()
//        setupNavigationBar()
//    }
//
//    private func setupNavigationBar() {
//        navigationController?.navigationBar.tintColor = .white
//        navigationItem.largeTitleDisplayMode = .never
//        let backButtonImage = UIImage(systemName: "arrow.left")?.withRenderingMode(.alwaysTemplate)
//        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action: #selector(backButtonTapped))
//
//        navigationItem.leftBarButtonItem = backButton
//        navigationItem.backBarButtonItem = nil
//    }
//
//    private func setupElement() {
//        progressIndicator.color = RMColor.dirtGreen
//
//        iconImageView.fetchImage(from: characterModel?.image ?? "")
//        iconImageView.layer.masksToBounds = true
//        iconImageView.layer.cornerRadius = Sizes.cornerRadius
//
//        fullNameLabel.moreSettings(text: characterModel.name ?? "", boldFont: true, alignment: .center)
//        statusLabel.moreSettings(text: characterModel.status?.rawValue ?? "", textColor: RMColor.dirtGreen, fontSize: Sizes.minFontSize, alignment: .center)
//
//        subviewOnView(progressIndicator, iconImageView, fullNameLabel, statusLabel, tableView)
//        constraints()
//    }
//
//    private func setupTableView() {
//        tableView.backgroundColor = RMColor.AdditionalColor.clear
//        tableView.separatorColor = RMColor.AdditionalColor.clear
//
//        tableView.delegate = self
//        tableView.dataSource = self
//
//        [InfoCell.self, OriginCell.self, EpisodeCell.self].forEach { cell in
//            tableView.register(cell, forCellReuseIdentifier: cell.description())
//        }
//    }
//
//    private func configureCell<T: UITableViewCell>(cell: T) -> T {
//        cell.backgroundColor = RMColor.AdditionalColor.clear
//        cell.selectionStyle = .none
//        return cell
//    }
//
//    private func fetchDataLocation(url: String, completion: @escaping(RMLocationModel) -> ()) {
//        viewModel.fetchDataLocation(url: url) { location in
//            completion(location)
//        }
//    }
//
//    private func fetchDataEpisode(url: String, completion: @escaping(RMEpisodeModel) -> ()) {
//        viewModel.fetchDataEpisode(url: url) { episode in
//            completion(episode)
//        }
//    }
//}
//
//private extension CharacterViewController {
//    private func subviewOnView(_ subviews: UIView...) {
//        subviews.forEach { subview in
//            view.addSubview(subview)
//        }
//    }
//
//    private func constraints() {
//        [progressIndicator, iconImageView, fullNameLabel, statusLabel, tableView].forEach { view in
//            view.translatesAutoresizingMaskIntoConstraints = false
//        }
//
//        NSLayoutConstraint.activate([
//            progressIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            progressIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
//            progressIndicator.heightAnchor.constraint(equalToConstant: Sizes.Padding.M.height),
//            progressIndicator.widthAnchor.constraint(equalToConstant: Sizes.Padding.M.width),
//
//            iconImageView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: Sizes.Spacing.L.top),
//            iconImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            iconImageView.heightAnchor.constraint(equalToConstant: Sizes.Padding.XL.height),
//            iconImageView.widthAnchor.constraint(equalTo: iconImageView.heightAnchor, multiplier: Sizes.Padding.XL.multiplier),
//
//            fullNameLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: Sizes.Padding.M.multiplier),
//            fullNameLabel.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: Sizes.Spacing.M.top),
//            fullNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//
//            statusLabel.widthAnchor.constraint(equalTo: iconImageView.widthAnchor, multiplier: Sizes.Padding.XL.multiplier),
//            statusLabel.topAnchor.constraint(equalTo: fullNameLabel.bottomAnchor, constant: Sizes.Spacing.S.top),
//            statusLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//
//            tableView.topAnchor.constraint(equalTo: statusLabel.bottomAnchor, constant: Sizes.Spacing.L.top),
//            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            tableView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: Sizes.Padding.XL.multiplier),
//            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
//        ])
//    }
//}
